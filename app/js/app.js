var phoneApp = angular.module('phoneApp', ['ui.router', 'ngResource', 'phonesModule', 'phoneModule', 'commitModule', 'orderModule', 'searcherModule'])

.controller('mainController', ['$scope', 'basketService', function ($scope, basketService) {
  $scope.orderArr = []; 
  
  $scope.basketService = basketService;
}]);