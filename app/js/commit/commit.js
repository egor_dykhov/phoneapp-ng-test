angular.module('commitModule', [])

.controller('commitController', [function () {
  this.commit = {};
    
  this.addCommit = function (commits) {
    commits.push(this.commit);
    
    this.commit = {};
  } 
}]);