angular.module('commitModule').directive('commit', function () {
  return {
    restrict: 'E',
    templateUrl: 'js/commit/templates/commit.htm',
    replace: true    
  };
});