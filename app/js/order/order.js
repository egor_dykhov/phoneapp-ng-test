angular.module('orderModule', [])
  
.controller('orderController', ['basketService', function (basketService) { 
  this.order = {};
  
  this.orders = [];
  
  this.addOrder = function () {
    this.orders.push(this.order);
    this.order = {};
    basketService.resetBasket();
  };
}]);