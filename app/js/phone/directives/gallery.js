angular.module('phoneModule').directive('gallery', function () {
  return {
    restrict: 'E',
    templateUrl: 'js/phone/templates/gallery.htm',
    replace: true,
    scope: {
      phoneObject: '=',
      getCurrentImg: '&',
      setCurrentImg: '&'
    }
  };
});