angular.module('phoneModule').directive('specifications', function () {
  return {
    restrict: 'E',
    templateUrl: 'js/phone/templates/specifications.htm',
    replace: true,
    scope: {
      phone: '='
    }
  };
});