angular.module('phoneModule', [])

.controller('phoneController', ['phoneService', '$stateParams', '$location', 'phonesService', '$scope', function (phoneService, $stateParams, $location, phonesService, $scope) { 
  this.phone = phoneService.getPhone($stateParams.id);  
  
  this.switchTo = function (direction) {
    var id = this.phone.id, 
        orderArr;
    
    if ($scope.$parent.orderArr.length) {
      orderArr = $scope.$parent.orderArr;
    } else {
      orderArr = jsonPhonesToArrayId(phonesService.getPhones());
    }
    
    if (direction === 'next') {
      id = getNextId(id, orderArr);  
    } else if (direction === 'previous') {
      id = getPreviousId(id, orderArr);   
    }
    
    $location.path('phone/' + id);
  };
  
  function getNextId (id, arr) {
    var currentIndex = arr.indexOf(id),
        nextIndex = currentIndex;
    
    if (currentIndex < arr.length - 1) {
      nextIndex = currentIndex + 1;
    }
    
    return arr[nextIndex];
  }
  
  function getPreviousId (id, arr) {
    var currentIndex = arr.indexOf(id),
        previousIndex = currentIndex;
    
    if (currentIndex > 0) {
      previousIndex = currentIndex - 1;
    }
    
    return arr[previousIndex];
  }
  
  function jsonPhonesToArrayId (jsonPhones) {
    var phones = jsonPhones,
        arrId = [],
        length = phones.length,
        i;
    
    for (i = 0; i < length; i++) {
      arrId.push(phones[i].id);
    }    
    
    return arrId;
  }
  
  this.currentImg = '';
  
  this.getCurrentImg = function () {
    if(!this.currentImg) {
      this.currentImg = this.phone.images? this.phone.images[0]: '';
    }   
    
    return this.currentImg;
  };
  
  this.setCurrentImg = function (imgUrl) {
    this.currentImg = imgUrl;    
  };    
  
  this.commits = [];       
}]);