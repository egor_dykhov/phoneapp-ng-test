angular.module('phonesModule', [])

.controller('phonesController', ['phonesService', '$scope', function (phonesService, $scope) {
  var _this = this;
  
  this.phones = phonesService.getPhones();  
  
  this.sort = '';  
  
  this.sortingByItem = function (item) {   
    (this.sort !== item)? this.sort = item: this.sort = '-' + item;
  };
  
  this.setOrder = function (index, id) {    
    $scope.$parent.orderArr[index] = id;        
  };
}]);