phoneApp.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  
  $stateProvider
    .state('phones', {
      url: '/',
      templateUrl: 'js/phones/templates/phones.htm',
      controller: 'phonesController',
      controllerAs: 'phonesCtrl'
    })
    .state('phone', {
      url: '/phone/:id',
      templateUrl: 'js/phone/templates/phone.htm',
      controller: 'phoneController',
      controllerAs: 'phoneCtrl'
    })
    .state('basket', {
      url: '/basket',
      templateUrl: 'js/basket/templates/basket.htm'
    })
    .state('order', {
      url: '/order',
      templateUrl: 'js/order/templates/order.htm',
      controller: 'orderController',
      controllerAs: 'orderCtrl'
    })
    .state('404', {
      url: '/:path',
      templateUrl: 'js/404/templates/404.htm'      
    })
    .state('about', {
      url: '/about' 
    })
    .state('contact', {
      url: '/contact'      
    });
});
