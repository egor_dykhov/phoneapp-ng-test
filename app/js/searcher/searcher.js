angular.module('searcherModule', [])
  
.controller('searcherController', ['$scope', '$location', function ($scope, $location) {
  $scope.$parent.searcher = {
    searchText: ''
  };  
  
  $scope.isDisabled = function () {
    var result = true;
    
    if ($location.path() === '/') {
      result = false;
    }    
    
    return result;
  };
}]);