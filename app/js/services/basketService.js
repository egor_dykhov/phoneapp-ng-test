phoneApp.service('basketService', [function () {  
  var products = [];
  
  this.getProducts = function () {
    return products;
  };
  
  this.removeProduct = function (productIndex) {
    products.splice(productIndex, 1);
  }; 
  
  this.addProductToBasket = function (product) {
    products.push(product);
  };
  
  this.getTotalPrice = function () {
    var length = products.length,
        total = 0,
        i;
    
    if (length) {
      for (i = 0; i < length; i++) {
        total = total + products[i].price;
      }
    }   
    
    return total;
  }; 
  
  this.resetBasket = function () {
    products = [];
  };
}]);