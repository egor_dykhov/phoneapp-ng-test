phoneApp.service('phoneService', ['$resource', function ($resource) {  
  this.getPhone = function (path) {
    var phoneRequest = $resource('./phones/' + path + '.json',{ }, {getData: {method:'GET', isArray: false}}),        
        phone = phoneRequest.getData();
    
    return phone;
  };
}]);