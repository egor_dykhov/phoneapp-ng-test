phoneApp.service('phonesService', ['$resource', function ($resource) { 
  var phonesRequest = $resource('./phones/phones.json',{ }, {getData: {method:'GET', isArray: true}}),        
      phones = phonesRequest.getData();
  
  this.getPhones = function () {    
    return phones;
  }
}]);