var express = require('express'),    
    bodyParser = require('body-parser'),
    app = express(),
    fs = require('fs'),
    phones = require('./app/phones/phones.json');    

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//app.use('/app/lib', express.static(__dirname + '/app/lib'));
app.use('/', express.static(__dirname + '/app/'));

app.get('/', function (request, response) {
  response.sendFile('index.html', {
    root: __dirname
  });
});

//app.get('/phones', function (request, response) {
//  response.json(phones);
//});
//
//app.get('/phones/:type', function (request, response) {
////  response.json(phones);
//});

//app.post('/addrule', function (request, response) {
//  var newRule = {
//    id: rulesCollection.length,
//    ruleName: request.body.ruleName    
//  };
//  
//  rulesCollection.push(newRule);  
//  
//  response.json(rulesCollection);
//});

app.listen(3000);

console.log("\nServer start on 127.0.0.1:3000\n");